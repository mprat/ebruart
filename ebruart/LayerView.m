//
//  LayerView.m
//  ebruart
//
//  Created by Michele on 3/22/14.
//  Copyright (c) 2014 Michele. All rights reserved.
//

#import "LayerView.h"

@implementation LayerView

- (id) initWithFrame:(CGRect)frame withContext:(EAGLContext *)context{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupLayer];
        [self setupRenderBufferWithContext:context];
        [self setupFrameBuffer];
    }
    return self;
}


- (IBAction)respondToLongTapGesture:(UILongPressGestureRecognizer *)recognizer{
    CGPoint pressloc = [recognizer locationInView:self];
    NSLog(@"%@", NSStringFromCGPoint(pressloc));
}

- (void)setupLayer {
    _eaglLayer = (CAEAGLLayer*) self.layer;
    _eaglLayer.opaque = YES;
}

- (void)setupRenderBufferWithContext:(EAGLContext*)context {
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];
}

- (void)setupFrameBuffer {
    GLuint framebuffer;
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER, _colorRenderBuffer);
}

+ (Class)layerClass {
    return [CAEAGLLayer class];
}

@end
