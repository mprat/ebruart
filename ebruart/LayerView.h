//
//  LayerView.h
//  ebruart
//
//  Created by Michele on 3/22/14.
//  Copyright (c) 2014 Michele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LayerView : UIView{
    CAEAGLLayer* _eaglLayer;
    GLuint _colorRenderBuffer;
}

- (id)initWithFrame:(CGRect)frame withContext:(EAGLContext*)context;

@end
