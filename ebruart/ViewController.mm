//
//  ViewController.m
//  ebruart
//
//  Created by Michele on 3/22/14.
//  Copyright (c) 2014 Michele. All rights reserved.
//

#import "ViewController.h"
#import "LayerView.h"
#import "SVGKit.h"
#import "SVGKLayeredImageView.h"
#import "SVGKSourceString.h"
#include "cpplinearalgebra.h"

// attribute index
enum{
    ATTRIB_POS
};

// uniform index
enum{
    UNIFORM_MVP
};

typedef struct{
    float coords[3];
} pt;

@interface ViewController (){
    GLuint _program;
    GLuint mvp_uniform;
    NSString* drawn;
    
    int startpan_x;
    int startpan_y;
}

@property (strong, nonatomic) EAGLContext *context;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) int nextRadius;
@end

@implementation ViewController

@synthesize context = _context;
@synthesize timer = _timer;
@synthesize nextRadius = _nextRadius;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
//    CGRect bounds = [[UIScreen mainScreen] bounds];
//    LayerView* firstlayer = [[LayerView alloc] initWithFrame:bounds withContext:self.context];
//    [self.view insertSubview:firstlayer atIndex:0];
    
    UILongPressGestureRecognizer *pressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(respondToLongTapGesture:)];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(respondToPanGesture:)];
    
    startpan_x = 0;
    startpan_y = 0;
    
    [self.view addGestureRecognizer:pressRecognizer];
    [self.view addGestureRecognizer:panRecognizer];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    if (!self.context) {
        NSLog(@"Failed to initialize OpenGLES 2.0 context");
        exit(1);
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        NSLog(@"Failed to set current OpenGL context");
        exit(1);
    }
    
//    CGRect bounds = [[UIScreen mainScreen] bounds];
//    LayerView* firstlayer = [[LayerView alloc] initWithFrame:bounds withContext:self.context];
//    [self.view insertSubview:firstlayer atIndex:0];
    
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    [self.context presentRenderbuffer:GL_RENDERBUFFER];
    
//    [self loadShaders];
//    [self setupVBOs];
//    [self drawCircle];
    
//    SVGKImage* newImage = [SVGKImage imageNamed:@"test"];

//    SVGKSource* s = [SVGKSourceString sourceFromContentsOfString:[self returnCircleSVGTextwithRadius:50 atX:100 atY:100]];
//    SVGKImage* newImage = [SVGKImage imageWithSource:s];
//    [self.view addSubview:[[SVGKLayeredImageView alloc] initWithSVGKImage:newImage]];
}

- (void)incrementCounter:(NSTimer *)timer {
    NSDictionary *userInfo = [timer userInfo];
    CGPoint pl = [[userInfo objectForKey:@"pressloc"] CGPointValue];
    self.nextRadius+=10;
    
    SVGKSource *s = [SVGKSourceString sourceFromContentsOfString:[self returnCircleWithBezierWithRadius: self.nextRadius atX:pl.x atY:pl.y]];
    SVGKImage* newImage = [SVGKImage imageWithSource:s];
    NSLog(@"%d", [[self.view subviews] count]);
    [self.view addSubview:[[SVGKLayeredImageView alloc] initWithSVGKImage:newImage]];
}

- (IBAction)respondToLongTapGesture:(UILongPressGestureRecognizer *)recognizer{
    CGPoint pressloc = [recognizer locationInView:self.view];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.nextRadius = 30;
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSValue valueWithCGPoint:pressloc], @"pressloc",
                                  nil];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(incrementCounter:) userInfo:userInfo repeats:true];
    }
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        [self.timer invalidate];
    }
//    NSLog(@"%@", NSStringFromCGPoint(pressloc));
 
    // TODO:
    // 1. change size based on time held
    // 2. re-draw on one layer, not add new layers
//    SVGKSource *s = [SVGKSourceString sourceFromContentsOfString:[self returnCircleSVGTextwithRadius:50 atX:pressloc.x atY:pressloc.y]];
    
}

- (IBAction)respondToPanGesture:(UIPanGestureRecognizer *)recognizer{
    if (recognizer.state == UIGestureRecognizerStateBegan){
        CGPoint trans = [recognizer locationInView:self.view];
        startpan_x = trans.x;
        startpan_y = trans.y;
    }
    if (recognizer.state == UIGestureRecognizerStateEnded){
        CGPoint trans = [recognizer locationInView:self.view];
        SVGKSource *s = [SVGKSourceString sourceFromContentsOfString:[self returnLineSVGFromX1:startpan_x andY1:startpan_y andX2:trans.x andY2:trans.y]];
        SVGKImage* newImage = [SVGKImage imageWithSource:s];
        [self.view addSubview:[[SVGKLayeredImageView alloc] initWithSVGKImage:newImage]];

        [recognizer setTranslation:CGPointZero inView:self.view];
        
        NSLog(@"pan ended");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*) returnCircleSVGTextwithRadius: (int)radius atX:(int)x atY: (int) y {
    return [NSString stringWithFormat:@"<svg width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">>/><circle cx=\"%d\" cy=\"%d\" r=\"%d\" fill=\"red\" /></svg>", 2*radius, 2*radius, x, y, radius];
}

- (NSString *) returnLineSVGFromX1: (int)x1 andY1:(int)y1 andX2:(int)x2 andY2:(int)y2 {
    return [NSString stringWithFormat:@"<svg width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">>/><line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" style=\"stroke:black\"/></svg>", abs(x1-x2), abs(y1-y2), x1, y1, x2, y2];
}

- (NSString *) returnCircleWithBezierWithRadius: (int)radius atX:(int)x atY: (int)y {
    return [NSString stringWithFormat:@"<svg width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">>/><path d=\"M %d,%d Q %d,%d %d,%d\"/><path d=\"M %d,%d Q %d,%d %d,%d\"/></svg>", 2*radius, 2*radius, int(x - 0.4*radius), y, x, int(y + 0.6*sqrt(2)*radius), int(x + 0.4*radius), y, int(x - 0.4*radius), y, x, int(y - 0.6*sqrt(2)*radius), int(x + 0.4*radius), y];
}

- (void) drawCircle{
    pt dots[10];
    float numpts = sizeof(dots)/(3*sizeof(float));
    float radius = 0.5;
    // 6 is the number of points
    for (int i = 0; i < numpts; i++) {
        float theta = 2.0f * 3.1415926f * i / numpts;//get the current angle
        
		float x = radius * cosf(theta);//calculate the x component
		float y = radius * sinf(theta);//calculate the y component
        
		pt temp = {x, y, 0};
        dots[i] = temp;
    }

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    // TODO: fix the hack about size
    glViewport(0, 0, self.view.frame.size.width,self.view.frame.size.height);
    
    Matrix4x4 mvp = Matrix4x4(1.0, 0, 0, 0,
                              0, 1.0, 0, 0,
                              0, 0, 1.0, 0,
                              0, 0, 0, 1.0);
    glUniformMatrix4fv(mvp_uniform, 1, GL_FALSE, (float *)mvp);
    
    glBufferData(GL_ARRAY_BUFFER, 3*sizeof(float)*numpts, dots, GL_STATIC_DRAW);
    glEnableVertexAttribArray(ATTRIB_POS);
    glVertexAttribPointer(ATTRIB_POS, 3, GL_FLOAT, GL_FALSE,
                          3*sizeof(float), 0);
    glDrawArrays(GL_POINTS, 0, numpts);
    
    [self.context presentRenderbuffer:GL_RENDERBUFFER];
    
    glDisableVertexAttribArray(ATTRIB_POS);
}

- (void) setupVBOs{
    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    //TODO: fix the size of the buffer that gets initialized
    glBufferData(GL_ARRAY_BUFFER, 3*sizeof(float)*10, NULL, GL_STATIC_DRAW);
}

- (BOOL) loadShaders{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    _program = glCreateProgram();
    
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"simple" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }
    
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"simple" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }
    
    glAttachShader(_program, vertShader);
    glAttachShader(_program, fragShader);
    
    // needs to happen before linking
    glBindAttribLocation(_program, ATTRIB_POS, "position");
    
    
    // Link program.
    if (![self linkProgram:_program]) {
        NSLog(@"Failed to link program: %d", _program);
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (_program) {
            glDeleteProgram(_program);
            _program = 0;
        }
        
        return NO;
    }
    
    // Get uniform locations
    mvp_uniform = glGetUniformLocation(_program, "modelViewProjectionMatrix");
    
    if (vertShader) {
        glDetachShader(_program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(_program, fragShader);
        glDeleteShader(fragShader);
    }
    
    glUseProgram(_program);
    
    return YES;
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

@end
