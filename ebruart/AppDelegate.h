//
//  AppDelegate.h
//  ebruart
//
//  Created by Michele on 3/22/14.
//  Copyright (c) 2014 Michele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
